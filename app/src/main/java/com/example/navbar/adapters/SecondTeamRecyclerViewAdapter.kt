package com.example.navbar.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.navbar.ActionType
import com.example.navbar.R
import com.example.navbar.databinding.SecondTeamItemLayoutBinding
import com.example.navbar.models.Summary
import com.example.navbar.models.TeamAction

class SecondTeamRecyclerViewAdapter: RecyclerView.Adapter<SecondTeamRecyclerViewAdapter.SecondTeamViewHolder>() {

    private val secondTeamItems = mutableListOf<TeamAction?>()

    inner class SecondTeamViewHolder(private val binding: SecondTeamItemLayoutBinding): RecyclerView.ViewHolder(binding.root){
        fun bind() {
            val secondTeamAction = secondTeamItems[adapterPosition]
            if (secondTeamAction != null) {
                when (secondTeamAction.actionType) {
                    ActionType.GOAL.type -> if (secondTeamAction.action?.goalType == 1) {
                        binding.secondActionType.text =
                            "Goal By"
                        binding.secondActionTypeIcon.setImageResource(R.drawable.ic_goal)
                        binding.playerName.text = secondTeamAction.action.player?.playerName
                    } else {
                        binding.secondActionType.text = "Own Goal"
                        binding.secondActionTypeIcon.setImageResource(R.drawable.ic_own_goal)
                        binding.playerName.text = secondTeamAction.action?.player?.playerName
                    }
                    ActionType.YELLOW_CARD.type -> {
                        binding.secondActionType.text = "Yellow Card"
                        binding.secondActionTypeIcon.setImageResource(R.drawable.ic_yellow_card)
                        binding.playerName.text = secondTeamAction.action?.player?.playerName
                    }
                    ActionType.SUBSTITUTION.type -> {
                        binding.secondActionType.text = "Substitution"
                        binding.secondActionTypeIcon.setImageResource(R.drawable.ic_subtitution)
                        binding.playerName.text = secondTeamAction.action?.player1?.playerName
                        binding.secondPlayerName.text = secondTeamAction.action?.player2?.playerName
                    }
                    ActionType.RED_CARD.type -> {
                        binding.secondActionType.text = "Red Card"
                        binding.secondActionTypeIcon.setImageResource(R.drawable.ic_red_card)
                        binding.playerName.text = secondTeamAction.action?.player1?.playerName
                        binding.secondPlayerName.text = secondTeamAction.action?.player2?.playerName
                    }
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SecondTeamViewHolder {
        return SecondTeamViewHolder(SecondTeamItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: SecondTeamViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = secondTeamItems.size

    fun addSecondTeamItems(items: List<TeamAction>?){
        if (items != null){
            secondTeamItems.addAll(items)
        }else{
            secondTeamItems.add(null)
        }
        notifyItemInserted(secondTeamItems.size)
    }
}