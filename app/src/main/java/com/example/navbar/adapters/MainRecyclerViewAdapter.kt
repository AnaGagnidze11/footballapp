package com.example.navbar.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.navbar.databinding.InnerRecyclerLayoutBinding
import com.example.navbar.models.Summary

class MainRecyclerViewAdapter: RecyclerView.Adapter<MainRecyclerViewAdapter.ItemViewHolder>() {

    private var items = emptyList<Summary>()
    private val viewPool = RecyclerView.RecycledViewPool()
    private lateinit var firstTeamAdapter: FirstTeamRecyclerViewAdapter
    private lateinit var secondTeamAdapter: SecondTeamRecyclerViewAdapter

    inner class ItemViewHolder(private val binding: InnerRecyclerLayoutBinding): RecyclerView.ViewHolder(binding.root){
        val firstTeamRecyclerView: RecyclerView = binding.firstTeamRecyclerView
        val secondTeamRecyclerView: RecyclerView = binding.secondTeamRecyclerView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(InnerRecyclerLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val firstTeamChildLayoutManager = LinearLayoutManager(holder.firstTeamRecyclerView.context)

        firstTeamAdapter = FirstTeamRecyclerViewAdapter()
        holder.firstTeamRecyclerView.layoutManager = firstTeamChildLayoutManager
        holder.firstTeamRecyclerView.adapter = firstTeamAdapter
        holder.firstTeamRecyclerView.setRecycledViewPool(viewPool)


        val secondTeamChildLayoutManager = LinearLayoutManager(holder.secondTeamRecyclerView.context)

        secondTeamAdapter = SecondTeamRecyclerViewAdapter()
        holder.secondTeamRecyclerView.layoutManager = secondTeamChildLayoutManager
        holder.secondTeamRecyclerView.adapter = secondTeamAdapter
        holder.secondTeamRecyclerView.setRecycledViewPool(viewPool)

        val action = items[position]
        if (action.team1Action == null && action.team2Action == null){
            firstTeamAdapter.addFirstTeamItems(null)
            secondTeamAdapter.addSecondTeamItems(null)
        }else if(action.team1Action == null && action.team2Action != null){
            firstTeamAdapter.addFirstTeamItems(null)
            secondTeamAdapter.addSecondTeamItems(action.team2Action)
        }else if(action.team1Action != null && action.team2Action == null){
            firstTeamAdapter.addFirstTeamItems(action.team1Action)
            secondTeamAdapter.addSecondTeamItems(null)
        }else{
            firstTeamAdapter.addFirstTeamItems(action.team1Action)
            secondTeamAdapter.addSecondTeamItems(action.team2Action)
        }

    }

    override fun getItemCount()= items.size


    fun addItems(newItems: List<Summary>){
        this.items = newItems
        notifyDataSetChanged()
    }
}