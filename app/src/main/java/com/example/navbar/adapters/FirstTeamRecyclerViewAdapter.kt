package com.example.navbar.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.navbar.ActionType
import com.example.navbar.R
import com.example.navbar.databinding.FirstTeamItemLayoutBinding
import com.example.navbar.models.Summary
import com.example.navbar.models.TeamAction

class FirstTeamRecyclerViewAdapter :
    RecyclerView.Adapter<FirstTeamRecyclerViewAdapter.FirstTeamViewHolder>() {

    private val firstTeamItems = mutableListOf<TeamAction?>()

    inner class FirstTeamViewHolder(private val binding: FirstTeamItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind() {
            val firstTeamAction = firstTeamItems[adapterPosition]
            if (firstTeamAction != null) {
                when (firstTeamAction.actionType) {
                    ActionType.GOAL.type -> if (firstTeamAction.action?.goalType == 1) {
                        binding.actionType.text =
                            "Goal By"
                        binding.actionTypeIcon.setImageResource(R.drawable.ic_goal)
                        binding.playerName.text = firstTeamAction.action.player?.playerName
                    } else {
                        binding.actionType.text = "Own Goal"
                        binding.actionTypeIcon.setImageResource(R.drawable.ic_own_goal)
                        binding.playerName.text = firstTeamAction.action?.player?.playerName
                    }
                    ActionType.YELLOW_CARD.type -> {
                        binding.actionType.text = "Yellow Card"
                        binding.actionTypeIcon.setImageResource(R.drawable.ic_yellow_card)
                        binding.playerName.text = firstTeamAction.action?.player?.playerName
                    }
                    ActionType.SUBSTITUTION.type -> {
                        binding.actionType.text = "Substitution"
                        binding.actionTypeIcon.setImageResource(R.drawable.ic_subtitution)
                        binding.playerName.text = firstTeamAction.action?.player1?.playerName
                        binding.secondPlayerName.text = firstTeamAction.action?.player2?.playerName
                    }
                    ActionType.RED_CARD.type -> {
                        binding.actionType.text = "Red Card"
                        binding.actionTypeIcon.setImageResource(R.drawable.ic_red_card)
                        binding.playerName.text = firstTeamAction.action?.player?.playerName
                    }
                }
            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FirstTeamViewHolder {
        return FirstTeamViewHolder(
            FirstTeamItemLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: FirstTeamViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = firstTeamItems.size

    fun addFirstTeamItems(items: List<TeamAction>?) {
        if (items != null){
            firstTeamItems.addAll(items)
        }else{
            firstTeamItems.add(null)
        }
        notifyItemInserted(firstTeamItems.size)
    }
}