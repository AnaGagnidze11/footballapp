package com.example.navbar

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.example.navbar.adapters.ViewPagerAdapter
import com.example.navbar.databinding.ActivityBottomNavBinding
import com.example.navbar.ui.dashboard.DashboardFragment
import com.example.navbar.ui.matches.MatchesFragment
import com.example.navbar.ui.notifications.NotificationsFragment

class BottomNavActivity : AppCompatActivity() {

    private lateinit var binding: ActivityBottomNavBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBottomNavBinding.inflate(layoutInflater)
        setContentView(binding.root)
        addFragments()
    }

    private fun addFragments(){
        val fragments = mutableListOf<Fragment>()
        fragments.add(MatchesFragment())
        fragments.add(DashboardFragment())
        fragments.add(NotificationsFragment())

        binding.viewPager.adapter = ViewPagerAdapter(fragments, supportFragmentManager, lifecycle)
        binding.viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback(){
            override fun onPageSelected(position: Int) {
                selectedMenuItem(position)
            }
        })

       binding.navView.setOnNavigationItemSelectedListener {
           when(it.itemId){
               R.id.navigation_matches -> binding.viewPager.currentItem = 0
               R.id.navigation_top_players -> binding.viewPager.currentItem = 1
               R.id.navigation_videos -> binding.viewPager.currentItem = 2
           }
           true
       }
    }

    private fun selectedMenuItem(position:Int){
        binding.navView.menu.getItem(position).isChecked = true

    }
}