package com.example.navbar.network

data class ResultControl<T>(
    val status: Status,
    val data: T? = null,
    val message: String? = null,
) {

    enum class Status {
        SUCCESS,
        ERROR,
    }

    companion object {
        fun <T> success(data: T): ResultControl<T> {
            return ResultControl(Status.SUCCESS, data)
        }

        fun <T> error(message: String): ResultControl<T> {
            return ResultControl(Status.ERROR, null, message)
        }
    }



}