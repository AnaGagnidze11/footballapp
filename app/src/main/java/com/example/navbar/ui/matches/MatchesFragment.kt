package com.example.navbar.ui.matches

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.navbar.adapters.MainRecyclerViewAdapter
import com.example.navbar.databinding.FragmentMatchesBinding
import com.example.navbar.network.ResultControl
import com.example.navbar.viewmodels.FootballViewModel

class MatchesFragment : Fragment() {

    private var _binding: FragmentMatchesBinding? = null
    private val binding get() = _binding!!

    private val viewModel: FootballViewModel by viewModels()

    private lateinit var adapter: MainRecyclerViewAdapter


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (_binding == null)
            _binding = FragmentMatchesBinding.inflate(inflater, container, false)

        viewModel.init()
        init()
        observe()
        return binding.root
    }

    private fun init(){
        adapter = MainRecyclerViewAdapter()
        binding.mainRecyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.mainRecyclerView.adapter = adapter
    }

    private fun observe() {
        viewModel._infoLiveData.observe(viewLifecycleOwner, {
            when (it.status) {
                ResultControl.Status.SUCCESS -> {
                    binding.stadiumName.text = it.data!!.match.stadiumAddress
                    binding.matchTime.text = "${it.data.match.matchTime.toString()}'"
                    binding.finalScores.text =
                        it.data.match.team1?.score.toString() + ":" + it.data.match.team2?.score.toString()
                    binding.firstTeamName.text = it.data.match.team1?.teamName
                    binding.secondTeamName.text = it.data.match.team2?.teamName
                    binding.firstTeamBallPossessionPRC.text =
                        it.data.match.team1?.ballPosition.toString()
                    binding.secondTeamBallPossessionPRC.text =
                        it.data.match.team2?.ballPosition.toString()
                    binding.progressHorizontal.progress = it.data.match.team1?.ballPosition!!
                    binding.matchDate.text = it.data.match.matchDate.toString()

                    adapter.addItems(it.data.match.matchSummary?.summaries!!)

                }
                ResultControl.Status.ERROR -> {
                    Toast.makeText(requireContext(), "${it.message}", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}