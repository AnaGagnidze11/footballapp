package com.example.navbar.models

import com.google.gson.annotations.SerializedName

data class TeamAction(
    @SerializedName("action")
    val action: Action?,
    @SerializedName("actionType")
    val actionType: Int?,
    @SerializedName("teamType")
    val teamType: Int?
)
