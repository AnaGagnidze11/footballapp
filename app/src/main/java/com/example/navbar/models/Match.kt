package com.example.navbar.models

import com.google.gson.annotations.SerializedName

data class Match(
    @SerializedName("matchDate")
    val matchDate: Long?,
    @SerializedName("matchSummary")
    val matchSummary: MatchSummary?,
    @SerializedName("matchTime")
    val matchTime: Float?,
    @SerializedName("stadiumAdress")
    val stadiumAddress: String?,
    @SerializedName("team1")
    val team1: Team?,
    @SerializedName("team2")
    val team2: Team?
)
