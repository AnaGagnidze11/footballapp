package com.example.navbar.models

import com.google.gson.annotations.SerializedName

data class Action(
    @SerializedName("goalType")
    val goalType: Int?,
    val player: Player?,
    val player1: Player?,
    val player2: Player?
)
