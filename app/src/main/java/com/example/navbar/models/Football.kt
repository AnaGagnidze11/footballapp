package com.example.navbar.models

import com.google.gson.annotations.SerializedName

data class Football(
    @SerializedName("resultCode")
    val resultCode: Int,
    @SerializedName("match")
    val match: Match)



