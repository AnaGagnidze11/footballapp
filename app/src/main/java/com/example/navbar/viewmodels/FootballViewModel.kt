package com.example.navbar.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.navbar.models.Football
import com.example.navbar.network.ResultControl
import com.example.navbar.network.RetrofitService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class FootballViewModel : ViewModel() {

    private val infoLiveData = MutableLiveData<ResultControl<Football>>().apply {
        mutableListOf<Football>()
    }

    val _infoLiveData: LiveData<ResultControl<Football>> = infoLiveData



    fun init() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                getFootballInfo()
            }
        }
    }


    private suspend fun getFootballInfo() {
        val result = RetrofitService.retrofitService.getMatchDetails()
        if (result.isSuccessful) {
            val info = result.body()
            info?.let {
                infoLiveData.postValue(ResultControl.success(it))
            }
        }else{
            infoLiveData.postValue(ResultControl.error(result.errorBody().toString()))
        }
    }
}