package com.example.navbar

enum class ActionType (val type: Int){
    GOAL(1),
    YELLOW_CARD(2),
    RED_CARD(3),
    SUBSTITUTION(4)
}

enum class GoalType(type: Int){
    GOAL(1),
    OWN_GOAL(2)
}

enum class MatchTeamType(type: Int){
    TEAM1(1),
    TEAM2(2)

}